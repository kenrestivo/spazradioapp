# SPAZ Radio Android App

A radio listening app for the [SPAZ streaming radio station](http://spaz.org), written in Clojure for Android.

## Obtaining

It's on the [Google Play Store here](https://play.google.com/store/apps/details?id=org.spaz.radio).

Or, if you use Cyanogenmod or otherwise don't use Google Play, you can download/run [the most recent binary release here](http://spaz.org/~ken/spazradio.apk).

## Building

```bash
lein droid build
```

See [lein-droid docs](https://github.com/clojure-android/lein-droid/wiki/Tutorial) for more details on how to build.

## License

Copyright © 2013-2015 ken restivo <ken@restivo.org>

Distributed under the Eclipse Public License, the same as Clojure.

### The net.clandroid.service code was copy/pasted shamelessly from [NightWeb](https://github.com/oakes/Nightweb), whose license is:
```
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
```

### The BitcoinIntegration library was copy/pasted shamelessly from [Android Bitcoin Wallet](https://github.com/schildbach/bitcoin-wallet), whose license is:
```
  Copyright 2012-2014 the original author or authors.
 
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
 
     http://www.apache.org/licenses/LICENSE-2.0
 
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
```
